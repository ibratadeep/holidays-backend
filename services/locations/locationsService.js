/**
 * @author: Backend Team
 * @Date: 02-03-2022
 */

var mongoose = require("mongoose");
var locations = mongoose.model("locations");

class LocationsService {

  async fetchLocation(object) {
    try {
      let obj = {
        name: object. name
      };
      let res = [];
      const result = await locations["citySearchAgg"](obj.name);
      console.log(result)
      if (result) {
        console.log("database fetched");
        result.map((data) => {
          res.push({
            name: data.name,
            state: data.state
            });
        });
        return { result: res };
      } else {
        console.log("Error in fetching data__");
        return "failed";
      }
    } catch (err) {
      console.log(err);
    }
  }
}
module.exports = LocationsService;