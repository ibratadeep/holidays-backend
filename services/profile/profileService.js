var mongoose = require("mongoose");
var users = mongoose.model("users");

class ProfileService {
  async register(createObj) {
    try {
      let obj = {
        name: createObj.name,
        username: createObj.u_name,
        password: createObj.pwd,
        active: createObj.st,
        email: createObj.email,
        phone: createObj.ph,
      };
      const result = await users.create(obj);
      if (result) {
        console.log("database inserted");
        return { result: result + "database inserted" };
      } else {
        console.log("Error in creating data____");
        return "failed";
      }
    } catch (err) {
      console.log(err);
    }
  }

  async viewProfile(object) {
    try {
      let obj = {
        username: object.u_name,
        password: object.pwd,
      };
      let res = [];
      const result = await users.find(obj);
      if (result) {
        console.log("database fetched");
        result.map((data) => {
          res.push({
            name: data.name,
            active: data.active,
            email: data.email,
            phone: data.phone,
          });
        });
        return { result: res };
      } else {
        console.log("Error in fetching data____");
        return "failed";
      }
    } catch (err) {
      console.log(err);
    }
  }

  async updateProfile(updateObj) {
    try {
      let obj = {
        name: updateObj.name,
        username: updateObj.u_name,
        password: updateObj.pwd,
        active: updateObj.st,
        email: updateObj.email,
        phone: updateObj.ph,
      };
      let id = { _id: updateObj.id };
      const result = await users.findByIdAndUpdate(id, { $set: obj });
      if (result) {
        console.log("database updated");
        return { result: result + "database updated" };
      } else {
        console.log("Error in updating data____");
        return "failed";
      }
    } catch (err) {
      console.log(err);
    }
  }

  async deleteProfile(object) {
    try {
      let obj = { _id: object.id };
      const result = await users.findByIdAndDelete(obj);
       if (result) {
        console.log("database deleted");
        return { "result ": result + "database deleted" };
      } else {
        console.log("Error in deleting data____");
        return "failed";
      }
    } catch (err) {
      console.log(err);
    }
  }
}
module.exports = ProfileService;
