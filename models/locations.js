/**
 * @author: Backend Team
 * @Date: 02-03-2022
 */

var mongoose = require('mongoose');
const { response } = require('../app');

var locationsSchema = new mongoose.Schema({
    id:{
        type:Number,
        required:true,
    },
    name:{type:String},
    state:{type:String}

},{autoIndex:false,versionKey:false});

locationsSchema.statics = {
	citySearchAgg: async function (f) {
        
		var key = new RegExp(f, 'i');
		 let aggregator=[
			{$match:{"name": key}}
		 ]
         console.log("City search Aggregator",aggregator)
 
	let res = undefined
	if (aggregator) {
	 	res=	await mongoose.models["locations"].aggregate(aggregator).exec()
	   console.log("res....",res)
	}
	    return res;
	 }
} 
mongoose.model("locations",locationsSchema,"locations")