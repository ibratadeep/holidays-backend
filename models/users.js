var mongoose = require('mongoose');

var usersSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    username:{type:String},
    password:{type:String},
    active:{type:Boolean},
    email:{type:String},
    phone:{type:Number}

},{autoIndex:false,versionKey:false});


mongoose.model("users",usersSchema,"users")