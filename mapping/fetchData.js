/**
 * @author: Backend Team
 * @Date: 02-03-2022
 */

let mongoose = require('mongoose')
module.exports.fetchRecords = function (reqBody, c) {
    let { model, filter, projection, option, aggregator, params } = reqBody
    console.log(req);
    if (aggregator) {
        let m = mongoose.model(model);
        let agg = m[aggregator](filter, option, params)
        console.log('agg is----',JSON.stringify(agg));
        if (agg) {
            mongoose.models[model].aggregate(agg).exec((err, docs) => {

                console.log('err------------',err);
                console.log('docs------------',docs);
                if (!err && docs && docs.length > 0) {
                    return c(null, docs);
                } else {
                    return c(err, null);
                }
            });
        } else {
            return c('error', null)
        }
    } else {
        mongoose.models[model].find(filter, projection, option).lean().exec((err, docs) => {
            if (!err && docs && docs.length > 0) {
                return c(null, docs);
            } else {
                return c(err, null);
            }

        })
    }
}