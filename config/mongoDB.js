/**
 * @author: Anil Kumar Ch
 * @Date: 24-06-2021
 */
// Bring Mongoose into the app
const mongoose = require('mongoose');
//const winston = require('winston');

// fetch MongoDB URL based on Environment
const DBURI = process.env.MONGO_DB_URI || 'mongodb://localhost:27017/holidays_sar';

// Create the mongo db  connection
mongoose.connect(DBURI, {
  useNewUrlParser: true,
  //useUnifiedTopology: true,
  //useCreateIndex: true
  // socketTimeoutMS: 30000,
  // keepAlive: true,
  // reconnectTries: 90
}, function (error) {
  if (error) {
    logger.info(error);
    process.exit(1);
  }
});


if (process.env.NODE_ENV === 'development') {
  mongoose.set('debug', true);
}

// // Below is the CONNECTION EVENTS


// When successfully connected
mongoose.connection.on('connected', function () {
  logger.info('Mongoose default connection open to ' + DBURI);
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  logger.info('Mongoose default connection error: ' + err);
  process.exit(1);
});

mongoose.connection.on('reconnectFailed', function (err) {
  logger.info('Mongoose default connection reconnectFailed: ' + err);
  process.exit(1);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  logger.info('Mongoose default connection disconnected');
  process.exit(1);
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    logger.info('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});
