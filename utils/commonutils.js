const { isNotNull } = require("./validators");
const moment = require('moment');

/**
 * 
 * @param {*} data 
 * @returns
 * @author: Rambabu  
 *  @date:26-10-2021  
 */
const convertarrystrtostr = function (data) {
    let final_res = "";
    if (isNotNull(data)) {
        let strarray = data.split(',');
        for (let str of strarray) {
            if (final_res == "") {
                final_res = JSON.stringify(str.trim());
            } else {
                final_res = final_res.trim() + "," + JSON.stringify(str.trim())
            }
        }
        return final_res;
    }
    return final_res;
}

const formatDate = function (dateString,dateformat) {
    console.log(dateString);
    if(dateString != undefined){
    let updatedDate = dateString?dateString :new Date();
    updatedDate = moment(updatedDate).format(dateformat);
    return updatedDate;
    }
    return null;
}

module.exports = {
    convertarrystrtostr,
    formatDate
};
