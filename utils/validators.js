/**
 * 
 * @param {*} status 
 * @returns
 * @author: Rambabu  
 * Date:26-10-2021 
 */
const isNotNull = function (data) {
  if (data !== null
    && data !== undefined
    && data !== 'null'
    && data !== 'undefined'
    && data !== '') {
    return true;
  } else {
    return false;
  }
}

/**
 * 
 * @param {*} status 
 * @returns
 * @author: Rambabu  
 * Date:26-10-2021 
 */
const isSuccessResponse = function (status) {
  if (isNotNull(status) && status === 200) {
    return true;
  } else {
    return false;
  }

}
 

/**
 * 
 * @param {*} arr 
 * @returns 
 * @author: Rambabu  
 * Date:26-10-2021 
 */
const isArrayNotEmpty = function (arr) {
  if (isNotNull(arr) && arr.length > 0) {
    return true;
  } else {
    return false;
  }

}

module.exports = {
  isNotNull: isNotNull,
  isSuccessResponse: isSuccessResponse,
  isArrayNotEmpty: isArrayNotEmpty
};
