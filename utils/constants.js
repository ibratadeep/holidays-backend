module.exports = Object.freeze({
    TRADER: 'Trader',
    BROKER: 'Broker',
    DOC_TYPE_PENDING: 'Pending',
    DOC_TYPE_VERIFIED: 'Verified',
    TEAM_ST_PENDING: 'Pending',
    NEWS_ST_PENDING: 'InActive',
    USER_TYPE_GUEST:'Guest_User'
});
