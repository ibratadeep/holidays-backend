/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description: Common Response Formatter
 */
 class Response {
    /**
     * Success response creator
     *
     * @param {Object<Any>} data
     * @static
     * @public
     */
    static success (data) {
      return {
        data: data,
        status: true,
        error: null
      };
    }
  
    /**
     * Error Response creator
     *
     * @param {String} message
     * @param {Object<Error>} trace
     * @static
     * @public
     */
    static error (trace = null, message = 'Error Encountered') {
      return {
        data: null,
        status: false,
        error: {
          trace,
          message
        }
      };
    }
  }
  
  module.exports = Response;
  