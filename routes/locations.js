/**
 * @author: Backend Team
 * @Date: 02-03-2022
 */

var express = require("express");
var router = express.Router();
const locationsController = require("../controllers/locationsController");

router.post("/city-fetch", locationsController.fetchLocation);

router.get("*", function (req, res) {
  res.send("Hello DB from location Route");
});

module.exports = router;