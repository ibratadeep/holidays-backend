/**
 * @author: Backend Team
 * @Date: 02-03-2022
 */

 var express = require("express");
 var router = express.Router();
 
 var profileRouter = require("./profile");
 var locationsRouter = require("./locations");
 
 router.use("/v1/profile", profileRouter);
 router.use("/v1/locations", locationsRouter);
 
 /**
  * @description: unknown Router call
  * @author: Backend Team
  * @date :02-03-2022
  */
 router.get("*", function (req, res) {
   res.send("Hello DB API Route");
 });
 
 module.exports = router;