var express = require("express");
var router = express.Router();
const profileController = require("../controllers/profileController");

router.post("/register", profileController.register);
router.get("/view-profile", profileController.viewProfile);
router.post("/update", profileController.updateProfile);
router.post("/delete-account", profileController.deleteProfile);

router.get("*", function (req, res) {
  res.send("Hello DB from profile Route");
});

module.exports = router;