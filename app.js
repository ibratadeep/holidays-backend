/**
 * @author: Backend Team
 * @since: 02-03-2022
 * @description: This Main Entry Page for the Project
 */
const path = require("path");
require("dotenv").config({ path: path.join(__dirname, "/.env") });

const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const compression = require("compression");
const cors = require("cors");
const morgan = require("morgan");
const winston = require("winston");

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true,
      level: "info",
    }),
  ],
  exitOnError: false,
});

global.logger = logger;
require("./models/users");
require("./models/locations");
const app = express();
const server = require("http").createServer(app);
server.listen(process.env.PORT || 6006);

logger.info("Server listining on PORT is---", process.env.PORT);

// resuming on uncaught exception means in application any where any exception came here it will be catch
process.on("uncaughtException", function (err) {
  logger.error("uncaughtException in app.js is------", err);
});

// for parsing application/json
app.use(bodyParser.json());
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// Parse Cookie header and populate req.cookies with an object keyed by the cookie names.
app.use(cookieParser());
// Compres all the response Body
app.use(compression());
// Cross-Origin Resource Sharing
app.use(cors({ origin: "*" }));

/**
 * @author: Backend Team
 * @since: 02-03-2022
 * @description:Initialize mysql connection here
 */

app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");
  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With, content-type, Authorization"
  );
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);
  // Pass to next layer of middleware
  next();
});

app.use(express.static(path.join(__dirname, "public")));

app.use("/api", require("./routes"));

app.get("*", function response(req, res) {
  res.status(404).send();
});

//Initializing MongoDB Connection and Redis connection
require("./config/mongoDB");

module.exports = app;
