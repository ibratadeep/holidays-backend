const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/holidays_sar")
.then(()=>console.log("Connection to mongoDB server is successful"))
.catch((err) => console.log(err));

// const usersSchema = new mongoose.Schema({
//     name:{
//         type:String,
//         required:true
//     },
//     username:String,
//     password:String,
//     active:Boolean,
//     email:String,
//     phone:Number

// })
// const users = new mongoose.model("users",usersSchema)

// //Creating document(s)
// const createDocument = async ()=>{
//     try{
//         const user1 = new users({
//             name:"Brataeep",
//             username:"brata",
//             password:"deep",
//             active:true,
//             email:"xyz",
//             phone:9998887776
//         })
        
//         const user2 = new users({
//             name:"Richa",
//             username:"richa",
//             password:"richa",
//             active:true,
//             email:"xyz",
//             phone:6667778881
//         })
//         const user3 = new users({
//             name:"Anil Sir",
//             username:"anil",
//             password:"sir",
//             active:true,
//             email:"xyz",
//             phone:8889997776
//         })
//         //const result = await user1.save()
//         const result = await users.insertMany([user1,user2,user3])
//         console.log(result)
//     }catch(err){
//         console.log(err);
//     }
// }
// //createDocument()

// //Reading document(s)
// const getDocument = async ()=>{
//     try{
//         const result = await users.find({_id:"6206038d02396cec8b79ebfd"})
//         console.log(result);

//     }catch(err){
//         console.log(err);
//     }
// }
// //getDocument()

// //Updating Document(s)
// const updateDocument = async (_id) =>{
//     try{
//         //const result = await users.updateOne({_id},{$set:{name:"Brataeep Swain"}});
//         const result = await users.findByIdAndUpdate({_id},{$set:{name:"Brataeep Swain"}});
//         console.log(result);
//     }catch(err){
//         console.log(err);
//     }    
// }
// //updateDocument("6205fa533a8e27c4cbb29412")

// //Deleting Document(s)
//        const deleteDocument = async (_id) =>{
//            try{
//             //    const result = await users.deleteOne({_id})
//             const result = await users.findByIdAndDelete({_id});;
//                console.log(result + " Successfully Deleted");
//            }catch(err){
//                console.log(err);
//            }
//        }
//       // deleteDocument("620602b85e161da8181c6394")


// async viewProfile(object){
//     try{
//         let obj = {                    
//                     username: object.u_name,
//                     password: object.pwd                   
//                 }
//                 let res =[]
//                users.find(obj,function(err,data){
//                     if(!err){
//                         console.log("database fetched");
//                         //res.push(data)
//                          //console.log(result);
//                          data.map((data)=>{
//                              res.push({
//                                  name:data.name,
//                                  active:data.active,
//                                  email:data.email,
//                                  phone:data.phone
//                              })
//                          })
//                          //console.log(res);
//                         return {"result":res};
//                     }else{
//                         console.log("Error in fetching data____",err);
//                         return "failed"
//                     }
//                 })
                

//     }catch(err){
//         console.log(err);
//     }
// }

      const busSchema = new mongoose.Schema({
        product:{type:String},
        total:{type:Number},
        customer:{type:String}
    
    })
    const bus = new mongoose.model("business",busSchema,"business")

    
//Creating document(s)
const createDocument = async ()=>{
    try{
        const data1 = new bus({
            product:"pizza",
            total:249,
            customer:"Chirag"
        })
        
        const data2 = new bus({
            product:"burger",
            total:369,
            customer:"Saurabh"
        })
        const data3 = new bus({
            product:"ice-cream",
            total:349,
            customer:"Raghav"
        })
        //const result = await data1.save()
        const result = await bus.insertMany([data1,data2,data3])
        console.log(result)
    }catch(err){
        console.log(err);
    }
}
createDocument()
//Reading document(s)
const getDocument = async ()=>{
    try{
        //const result = await bus.count({product:"pizza"})
        //const result = await bus.distinct("product")
        const result = await bus.aggregate([{$match:{customer:{$in:["Anil","Richa"]}}},{$group:{_id:"$customer", total:{$sum:"$total"}}}])

        console.log(result);

    }catch(err){
        console.log(err);
    }
}
//getDocument()