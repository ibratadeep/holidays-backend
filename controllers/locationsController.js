/**
 * @author: Backend Team
 * @Date: 02-03-2022
 */

const LocationsService = require("../services/locations/locationsService");
const Response = require("../utils/Response");

const fetchLocation = async function (req, res) {
  try {
    const object = req.body;
    const locationsService = new LocationsService();
    const result = await locationsService.fetchLocation(object);
    res.send(result);
  } catch (e) {
    logger.error(e);
    res.status(404).send(Response.error(e));
  }
};
module.exports = {
fetchLocation
};