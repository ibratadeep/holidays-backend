const ProfileService = require("../services/profile/profileService");
const Response = require("../utils/Response");

const register = async function (req, res) {
  try {
    const createObj = req.body;
    const profileService = new ProfileService();
    const result = await profileService.register(createObj);
    res.send(result);
  } catch (e) {
    logger.error(e);
    res.status(404).send(Response.error(e));
  }
};

const viewProfile = async function (req, res) {
  try {
    const object = req.body;
    const profileService = new ProfileService();
    const result = await profileService.viewProfile(object);
    //console.log(result);
    res.send(result);
  } catch (e) {
    logger.error(e);
    res.status(404).send(Response.error(e));
  }
};

const updateProfile = async function (req, res) {
  try {
    const updateObj = req.body;
    const profileService = new ProfileService();
    const result = await profileService.updateProfile(updateObj);
    res.send(result);
  } catch (e) {
    logger.error(e);
    res.status(404).send(Response.error(e));
  }
};

const deleteProfile = async function (req, res) {
  try {
    const deleteObj = req.body;
    const profileService = new ProfileService();
    const result = await profileService.deleteProfile(deleteObj);
    res.send(result);
  } catch (e) {
    logger.error(e);
    res.status(404).send(Response.error(e));
  }
};

module.exports = {
  register,
  viewProfile,
  updateProfile,
  deleteProfile,
};
